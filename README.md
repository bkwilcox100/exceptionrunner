# Answers to Lab Logger with JUnit

1. The logger object is writing directly to the logger.log file instead of displaying it to console
2. It comes from the default Java logger
3. Assertions.assertThrows() asserts that a given function throws a given exception
4. i. Serial Version UID is used for serializing and deserializing objects. It is used to identify certain classes so that if a change is made, you can change the serial version UID and a previously serialized object can be deserialized without throwing an error. 
<br>ii. It allows us to throw a more specific exception or not throw one at all.
<br> iii. These are the only ones that we use.
5. The static block runs before the constructor, but only once.
6. README.md is a markdown file that gives information about a project. It is noramlly used in VCS repositories like bitbucket, github, and gitlab to display information.
7. The test was failing because a the timeToWait was never initialized and ended up causing a null pointer exception. To fix, just initialize timeToWait with a default value and it works.
8. The problem had to do with the program trying to use a null object. Null objects don't have any information to give so trying to access them will always throw an exception.
9. Image Attached
10. Image Attached
11. TimerException is an checked exception. NullPointerException is also an unchecked RuntimeException that describes when we are trying to access an object with a null value